# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/spumote/Documents/mipt/8sem/graphics/repo/seminar2/Sample_02_5_ScreenSpaceQuad.cpp" "/home/spumote/Documents/mipt/8sem/graphics/repo/seminar2/CMakeFiles/Sample_02_5_ScreenSpaceQuad.dir/Sample_02_5_ScreenSpaceQuad.cpp.o"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/common/Application.cpp" "/home/spumote/Documents/mipt/8sem/graphics/repo/seminar2/CMakeFiles/Sample_02_5_ScreenSpaceQuad.dir/__/common/Application.cpp.o"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/common/Camera.cpp" "/home/spumote/Documents/mipt/8sem/graphics/repo/seminar2/CMakeFiles/Sample_02_5_ScreenSpaceQuad.dir/__/common/Camera.cpp.o"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/common/DebugOutput.cpp" "/home/spumote/Documents/mipt/8sem/graphics/repo/seminar2/CMakeFiles/Sample_02_5_ScreenSpaceQuad.dir/__/common/DebugOutput.cpp.o"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/common/Mesh.cpp" "/home/spumote/Documents/mipt/8sem/graphics/repo/seminar2/CMakeFiles/Sample_02_5_ScreenSpaceQuad.dir/__/common/Mesh.cpp.o"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/common/ShaderProgram.cpp" "/home/spumote/Documents/mipt/8sem/graphics/repo/seminar2/CMakeFiles/Sample_02_5_ScreenSpaceQuad.dir/__/common/ShaderProgram.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_STATIC"
  "GLFW_DLL"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "common"
  "external/Assimp/include"
  "external/glew-1.13.0/include"
  "external/GLFW/include"
  "external/GLM"
  "external/imgui"
  "external/SOIL/src/SOIL2"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/spumote/Documents/mipt/8sem/graphics/repo/external/Assimp/code/CMakeFiles/assimp.dir/DependInfo.cmake"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/external/glew-1.13.0/build/cmake/CMakeFiles/glew_s.dir/DependInfo.cmake"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/external/imgui/CMakeFiles/imgui.dir/DependInfo.cmake"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/external/SOIL/CMakeFiles/soil.dir/DependInfo.cmake"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/external/Assimp/contrib/irrXML/CMakeFiles/IrrXML.dir/DependInfo.cmake"
  "/home/spumote/Documents/mipt/8sem/graphics/repo/external/GLFW/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
