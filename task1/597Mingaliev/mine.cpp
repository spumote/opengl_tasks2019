#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>

/**
Несколько примеров шейдеров
*/

void addSurface(double x1, double y1, double z1,
                double x2, double y2, double z2, 
                double x3, double y3, double z3,
                double x4, double y4, double z4,
                double r, double g, double b,
                std::vector<glm::vec3>& vertices,
                std::vector<glm::vec3>& normals,
                std::vector<glm::vec2>& texcoords)
{
    vertices.push_back(glm::vec3(x1, y1, z1));
    vertices.push_back(glm::vec3(x2, y2, z2));
    vertices.push_back(glm::vec3(x3, y3, z3));
    vertices.push_back(glm::vec3(x1, y1, z1));
    vertices.push_back(glm::vec3(x3, y3, z3));
    vertices.push_back(glm::vec3(x4, y4, z4));

    for (int i = 0; i < 6; i++)
        normals.push_back(glm::vec3(r / 255, g / 255, b / 255));
    
    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(0.0, 0.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));
}

MeshPtr makeLab()
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    //floor
    addSurface(0.0, 0.0, 0.0,
               12.0, 0.0, 0.0,
               12.0, 12.0, 0.0,
               0.0, 12.0, 0.0,
               0, 0, 127,
               vertices, normals, texcoords);

    //walls
    addSurface(2.0, 0.0, 0.0,
               12.0, 0.0, 0.0,
               12.0, 0.0, 2.0,
               2.0, 0.0, 2.0,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(0.0, 0.0, 0.0,
               0.0, 12.0, 0.0,
               0.0, 12.0, 2.0,
               0.0, 0.0, 2.0,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(0.0, 12.0, 0.0,
               12.0, 12.0, 0.0,
               12.0, 12.0, 2.0,
               0.0, 12.0, 2.0,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(12.0, 0.0, 0.0,
               12.0, 12.0, 0.0,
               12.0, 12.0, 2.0,
               12.0, 0.0, 2.0,
               255, 0, 0,
               vertices, normals, texcoords);

    //left side
    addSurface(2, 0, 0,
               2, 4, 0,
               2, 4, 2,
               2, 0, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(2, 4, 0,
               6, 4, 0,
               6, 4, 2,
               2, 4, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(6, 4, 0,
               6, 2, 0,
               6, 2, 2,
               6, 4, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(6, 2, 0,
               4, 2, 0,
               4, 2, 2,
               6, 2, 2,
               255, 0, 0,
               vertices, normals, texcoords);

    //bottom side
    addSurface(0, 6, 0,
               10, 6, 0,
               10, 6, 2,
               0, 6, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(10, 4, 0,
               10, 10, 0,
               10, 10, 2,
               10, 4, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(10, 4, 0,
               8, 4, 0,
               8, 4, 2,
               10, 4, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(8, 4, 0,
               8, 2, 0,
               8, 2, 2,
               8, 4, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(10, 0, 0,
               10, 2, 0,
               10, 2, 2,
               10, 0, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(10, 10, 0,
               8, 10, 0,
               8, 10, 2,
               10, 10, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(8, 10, 0,
               8, 8, 0,
               8, 8, 2,
               8, 10, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    
    //right side
    addSurface(6, 8, 0,
               6, 12, 0,
               6, 12, 2,
               6, 8, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(4, 8, 0,
               6, 8, 0,
               6, 8, 2,
               4, 8, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(4, 8, 0,
               4, 10, 0,
               4, 10, 2,
               4, 8, 2,
               255, 0, 0,
               vertices, normals, texcoords);
    addSurface(2, 8, 0,
               2, 12, 0,
               2, 12, 2,
               2, 8, 2,
               255, 0, 0,
               vertices, normals, texcoords);


    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Lab is created with " << vertices.size() << " vertices\n";

    return mesh;
}

class SampleApplication : public Application
{
public:
    MeshPtr _cube;
    MeshPtr _bunny;
    int cameraSwitcher = 0;
    CameraMoverPtr movOrbit, movFree;

    ShaderProgramPtr _shader;

    GLuint _ubo;

    GLuint uniformBlockBinding = 0;
    
    void updateGUI() override
    {
        if (cameraSwitcher == 0)
            _cameraMover = movOrbit;
        else
            _cameraMover = movFree;

        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("Camera Switcher", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::RadioButton("orbit", &cameraSwitcher, 0);
            ImGui::RadioButton("free", &cameraSwitcher, 1);
        }
        ImGui::End();
    }

    void makeScene() override
    {
        movOrbit = std::make_shared<OrbitCameraMover>();
        movFree = std::make_shared<FreeCameraMover>();
        Application::makeScene();

        _cube = makeLab();
        _cube->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //_bunny = loadFromFile("models/teapot_mine.obj");
        _bunny = makeSphere(0.5, 20);
        _bunny->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 1.0f, 0.0f)));

        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("shaders2/shaderUBO.vert", "shaders2/shader.frag");

        //=========================================================
        //Инициализация Uniform Buffer Object

	    // Выведем размер Uniform block'а.
	    GLint uniformBlockDataSize;
	    glGetActiveUniformBlockiv(_shader->id(), 0, GL_UNIFORM_BLOCK_DATA_SIZE, &uniformBlockDataSize);
	    std::cout << "Uniform block 0 data size = " << uniformBlockDataSize << std::endl;

        if (USE_DSA) {
            glCreateBuffers(1, &_ubo);
            glNamedBufferData(_ubo, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
        }
        else {
	        glGenBuffers(1, &_ubo);
	        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
	        glBufferData(GL_UNIFORM_BUFFER, uniformBlockDataSize, nullptr, GL_DYNAMIC_DRAW);
	        glBindBuffer(GL_UNIFORM_BUFFER, 0);
        }
	    // Привязываем буффер к точке привязки Uniform буферов.
        glBindBufferBase(GL_UNIFORM_BUFFER, uniformBlockBinding, _ubo);


        // Получение информации обо всех uniform-переменных шейдерной программы.
	    if (USE_INTERFACE_QUERY) {
		    GLsizei uniformsCount;
		    GLsizei maxNameLength;
		    glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_ACTIVE_RESOURCES, &uniformsCount);
		    glGetProgramInterfaceiv(_shader->id(), GL_UNIFORM, GL_MAX_NAME_LENGTH, &maxNameLength);
		    std::vector<char> nameBuffer(maxNameLength);

		    std::vector<GLenum> properties = {GL_TYPE, GL_ARRAY_SIZE, GL_OFFSET, GL_BLOCK_INDEX};
		    enum Property {
			    Type,
			    ArraySize,
			    Offset,
			    BlockIndex
		    };
		    for (GLuint uniformIndex = 0; uniformIndex < uniformsCount; uniformIndex++) {
			    std::vector<GLint> params(properties.size());
			    glGetProgramResourceiv(_shader->id(), GL_UNIFORM, uniformIndex, properties.size(), properties.data(),
					    params.size(), nullptr, params.data());
			    GLsizei realNameLength;
			    glGetProgramResourceName(_shader->id(), GL_UNIFORM, uniformIndex, maxNameLength, &realNameLength, nameBuffer.data());

			    std::string uniformName = std::string(nameBuffer.data(), realNameLength);

			    std::cout << "Uniform " << "index = " << uniformIndex << ", name = " << uniformName << ", block = " << params[BlockIndex] << ", offset = " << params[Offset] << ", array size = " << params[ArraySize] << ", type = " << params[Type] << std::endl;
		    }
	    }
	    else {
		    GLsizei uniformsCount;
		    glGetProgramiv(_shader->id(), GL_ACTIVE_UNIFORMS, &uniformsCount);

		    std::vector<GLuint> uniformIndices(uniformsCount);
		    for (int i = 0; i < uniformsCount; i++)
			    uniformIndices[i] = i;
		    std::vector<GLint> uniformBlocks(uniformsCount);
		    std::vector<GLint> uniformNameLengths(uniformsCount);
		    std::vector<GLint> uniformTypes(uniformsCount);
		    glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_BLOCK_INDEX, uniformBlocks.data());
		    glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_NAME_LENGTH, uniformNameLengths.data());
		    glGetActiveUniformsiv(_shader->id(), uniformsCount, uniformIndices.data(), GL_UNIFORM_TYPE, uniformTypes.data());

		    for (int i = 0; i < uniformsCount; i++) {
			    std::vector<char> name(uniformNameLengths[i]);
			    GLsizei writtenLength;
			    glGetActiveUniformName(_shader->id(), uniformIndices[i], name.size(), &writtenLength, name.data());
			    std::string uniformName = name.data();

			    std::cout << "Uniform " << "index = " << uniformIndices[i] << ", name = " << uniformName << ", block = " << uniformBlocks[i] << ", type = " << uniformTypes[i] << std::endl;
		    }
	    }
    }

    void update() override
    {
        Application::update();

        //Обновляем содержимое Uniform Buffer Object

#if 0
        //Вариант с glMapBuffer
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        GLvoid* p = glMapBuffer(GL_UNIFORM_BUFFER, GL_WRITE_ONLY);
        memcpy(p, &_camera, sizeof(_camera));
        glUnmapBuffer(GL_UNIFORM_BUFFER);
#elif 0
        //Вариант с glBufferSubData
        glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
        glBufferSubData(GL_UNIFORM_BUFFER, 0, sizeof(_camera), &_camera);
#else
        //Вариант для буферов, у которых layout отличается от std140

        //Имена юниформ-переменных
        const char* names[2] =
        {
            "viewMatrix",
            "projectionMatrix"
        };

        GLuint index[2];
        GLint offset[2];

        //Запрашиваем индексы 2х юниформ-переменных
        glGetUniformIndices(_shader->id(), 2, names, index);

        //Зная индексы, запрашиваем сдвиги для 2х юниформ-переменных
        glGetActiveUniformsiv(_shader->id(), 2, index, GL_UNIFORM_OFFSET, offset);

	    // Вывод оффсетов.
	    static bool hasOutputOffset = false;
	    if (!hasOutputOffset) {
		    std::cout << "Offsets: viewMatrix " << offset[0] << ", projMatrix " << offset[1] << std::endl;
		    hasOutputOffset = true;
	    }

        //Устанавливаем значения 2х юниформ-перменных по отдельности
	    if (USE_DSA) {
		    glNamedBufferSubData(_ubo, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
		    glNamedBufferSubData(_ubo, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
	    }
	    else {
		    glBindBuffer(GL_UNIFORM_BUFFER, _ubo);
		    glBufferSubData(GL_UNIFORM_BUFFER, offset[0], sizeof(_camera.viewMatrix), &_camera.viewMatrix);
		    glBufferSubData(GL_UNIFORM_BUFFER, offset[1], sizeof(_camera.projMatrix), &_camera.projMatrix);
	    }
#endif
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        unsigned int blockIndex = glGetUniformBlockIndex(_shader->id(), "Matrices");
        glUniformBlockBinding(_shader->id(), blockIndex, uniformBlockBinding);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _cube->modelMatrix());
        _cube->draw();

        _shader->setMat4Uniform("modelMatrix", _bunny->modelMatrix());
        _bunny->draw();
    }

    SampleApplication()
    {

    }
};

int main()
{
    SampleApplication app;

    app.start();

    return 0;
}
